#-----------------------------------------------------------------------------------------------------------------------
# Ship Behavior
#-----------------------------------------------------------------------------------------------------------------------

class_name AsteroidLevel extends Node3D

#-----------------------------------------------------------------------------------------------------------------------
# Editor Variables
#-----------------------------------------------------------------------------------------------------------------------

@export
var levelSize : Vector2 = Vector2(10000, 10000)

#-----------------------------------------------------------------------------------------------------------------------
# Godot Methods
#-----------------------------------------------------------------------------------------------------------------------

# Called when the node enters the scene tree for the first time.
func _ready():
    # Add to our level group
    self.add_to_group("level")

    # TODO: We probably will need a better way than hard coding the starting units in the level. Eh, consider this an
    # example of how to load these things.

    # Add the player units to the level
    var ship1 = load("res://scenes/ships/aegis-class.tscn").instantiate()
    ship1.position = Vector3(0, 0, 0)
    ship1.rotation_degrees = Vector3(0, 180, 0)
    ship1.add_to_group("selected")
    self.add_child(ship1)

    var ship2 = load("res://scenes/ships/helios-class.tscn").instantiate()
    ship2.position = Vector3(400, 0, 1100)
    ship2.rotation_degrees = Vector3(0, 180, 0)
    # ship2.add_to_group("selected")
    self.add_child(ship2)

    var ship3 = load("res://scenes/ships/helios-class.tscn").instantiate()
    ship3.position = Vector3(-400, 0, 1100)
    ship3.rotation_degrees = Vector3(0, 180, 0)
    # ship3.add_to_group("selected")
    self.add_child(ship3)

    var ship4 = load("res://scenes/ships/phobos-class.tscn").instantiate()
    ship4.position = Vector3(800, 0, 1600)
    ship4.rotation_degrees = Vector3(0, 180, 0)
    # ship4.add_to_group("selected")
    self.add_child(ship4)

    var ship5 = load("res://scenes/ships/phobos-class.tscn").instantiate()
    ship5.position = Vector3(400, 0, 1600)
    ship5.rotation_degrees = Vector3(0, 180, 0)
    # ship5.add_to_group("selected")
    self.add_child(ship5)

    var ship6 = load("res://scenes/ships/phobos-class.tscn").instantiate()
    ship6.position = Vector3(-400, 0, 1600)
    ship6.rotation_degrees = Vector3(0, 180, 0)
    # ship6.add_to_group("selected")
    self.add_child(ship6)

    var ship7 = load("res://scenes/ships/phobos-class.tscn").instantiate()
    ship7.position = Vector3(-800, 0, 1600)
    ship7.rotation_degrees = Vector3(0, 180, 0)
    # ship7.add_to_group("selected")
    self.add_child(ship7)

#-----------------------------------------------------------------------------------------------------------------------
