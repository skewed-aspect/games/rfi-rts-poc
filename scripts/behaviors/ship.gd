#-----------------------------------------------------------------------------------------------------------------------
# Ship Behavior
#-----------------------------------------------------------------------------------------------------------------------

class_name Ship extends Unit

#-----------------------------------------------------------------------------------------------------------------------

var _moveTo : Vector3 = Vector3.ZERO
var _flankSpeed : bool = false

enum OffensivePosture {AtWill, AsOrdered, Defensive, StandDown}
enum DefensivePosture {Offensive, Nominal, Defensive, FullDefensive}

## The collision shape for this ship
@onready
var collider : CollisionShape3D = $CollisionShape3D

#-----------------------------------------------------------------------------------------------------------------------
# Ship Attributes
#-----------------------------------------------------------------------------------------------------------------------

@export_range(0, 100)
var attack : int = 10

@export_range(0, 100)
var soak : int = 0

@export
var offensePosture : OffensivePosture = OffensivePosture.AtWill

@export
var defensePosture : DefensivePosture = DefensivePosture.Nominal

# Physical attributes

@export_range(0, 10000)
var maxNominalLinearSpeed : float = 100

@export_range(0, 10000)
var maxFlankLinearSpeed : float = 150

@export_range(0, 10000)
var maxNominalAngularSpeed : float = 100

@export_range(0, 10000)
var maxFlankAngularSpeed : float = 150

@export_range(0, 1000000)
var maxNominalLinearAccel : float = 100

@export_range(0, 1000000)
var maxNominalAngularAccel : float = 100

@export_range(0, 1000000)
var maxFlankLinearAccel : float = 100

@export_range(0, 1000000)
var maxFlankAngularAccel : float = 100

# Steering

@export_range(0, 1000000)
var arrivalTolerance : float

## The tolerance for the ship's alignment to the target in degrees
@export_range(0, 1000000)
var alignmentTolerance : float = 10

#-----------------------------------------------------------------------------------------------------------------------
# Properties
#-----------------------------------------------------------------------------------------------------------------------

## The maximum linear speed of the ship in meters per second
var maxLinearSpeed : float:
    get:
        if self.flankSpeed:
            return self.maxFlankLinearSpeed
        else:
            return self.maxNominalLinearSpeed

## The maximum angular speed of the ship in degrees per second
var maxAngularSpeed : float:
    get:
        if self.flankSpeed:
            return self.maxFlankAngularSpeed
        else:
            return self.maxNominalAngularSpeed

## The maximum linear acceleration of the ship in meters per second squared
var maxLinearAccel : float:
    get:
        if self.flankSpeed:
            return self.maxFlankLinearAccel
        else:
            return self.maxNominalLinearAccel
## The maximum angular acceleration of the ship in degrees per second squared
var maxAngularAccel : float:
    get:
        if self.flankSpeed:
            return self.maxFlankAngularAccel
        else:
            return self.maxNominalAngularAccel

## The radius of the ship's collision shape
var collisionRadius : float:
    get:
        return self._calculateRadius(self.collider.shape.points)

## If true, the ship will move at it's maximum possible speed.
@export
var flankSpeed : bool:
    get:
        return self._flankSpeed
    set(value):
        self._flankSpeed = value


#-----------------------------------------------------------------------------------------------------------------------
# Internal Utils
#-----------------------------------------------------------------------------------------------------------------------

func _calculateRadius(polygon : PackedVector3Array) -> float:
    var furthestPoint := Vector3(-INF, -INF, -INF)
    for p in polygon:
        if abs(p.x) > furthestPoint.x:
            furthestPoint.x = p.x
        if abs(p.y) > furthestPoint.y:
            furthestPoint.y = p.y
        if abs(p.z) > furthestPoint.z:
            furthestPoint.z = p.z

    # Calculate the length of the vector from the origin to the furthest point taking translation into account. Then  we ceil it to round up and get a nicer number to work with.
    return ceil(self.collider.position.distance_to(furthestPoint))

#-----------------------------------------------------------------------------------------------------------------------
# Commands
#-----------------------------------------------------------------------------------------------------------------------

func _move(args : Dictionary) -> void:
    prints("Moving to " + str(args.pos))
    self._moveTo = args.pos

#-----------------------------------------------------------------------------------------------------------------------

func _init() -> void:
    # Add to group
    self.add_to_group("ships")

    # Disable our ability to sleep
    self.can_sleep = false

    # Default some attributes
    self.disabledHealth = 0
    self.destroyedHealth = -(ceil(self.maxHealth / 2.0))

    # Register commands
    self._commandFuncs['move'] = self._move

func _ready() -> void:
    prints("[" + self.name + "] Collision radius:", str(self.collisionRadius))

    # Set the initial move target to our initial position
    self._moveTo = position

func _physics_process(delta: float) -> void:
    var direction = (self._moveTo - self.position).normalized()
    var distance = self.position.distance_to(self._moveTo)

    var local_target_direction = self.to_local(self._moveTo)
    var angle_to_target = atan2(local_target_direction.x, local_target_direction.z)

    # Check if we need to reorient towards the target or upwards
    if abs(angle_to_target) > deg_to_rad(self.alignmentTolerance) or self.global_transform.basis.y.dot(Vector3.UP) < 0.99:
        var desired_angular_velocity = Vector3.UP * sign(angle_to_target) * deg_to_rad(self.maxAngularSpeed)
        var damping = 0.1
        self.angular_velocity = self.angular_velocity.lerp(desired_angular_velocity, damping)

        # Apply torque to reorient upwards if necessary
        if self.global_transform.basis.y.dot(Vector3.UP) < 0.99:
            var torque = (Vector3.UP - self.global_transform.basis.y) * self.maxAngularAccel
            self.apply_torque(torque)
    else:
        self.angular_velocity = Vector3.ZERO

    if abs(angle_to_target) <= PI / 4:
        if distance <= self.arrivalTolerance:
            self.linear_velocity = Vector3.ZERO
        else:
            var deceleration_distance = (self.linear_velocity.length_squared() - 0) / (2 * self.maxLinearAccel)
            var desired_velocity = direction * self.maxLinearSpeed
            if distance < deceleration_distance:
                desired_velocity *= distance / deceleration_distance
            var force = (desired_velocity - self.linear_velocity) / delta
            self.apply_central_force(force)
    else:
        self.linear_velocity = Vector3.ZERO

#-----------------------------------------------------------------------------------------------------------------------
