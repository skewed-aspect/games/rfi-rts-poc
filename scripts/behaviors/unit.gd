#-----------------------------------------------------------------------------------------------------------------------
# RTS Unit
#-----------------------------------------------------------------------------------------------------------------------

class_name Unit extends RigidBody3D
## A unit in the game. A unit is a single entity that can be controlled by the player.
##
## Units can be selected, given commands, and can be destroyed.
##
## Units are made up of a model, health, and command handling logic.

#-----------------------------------------------------------------------------------------------------------------------

## The command handler function type. This is used to register command handlers. Subclasses should register their own
## command handlers with:
##
##     _commandFuncs["command"] = _commandHandlerFunc
##
## The command handler function should have the format:
##
##     func _commandHandlerFunc(args : Dictionary) -> void:
##         ...
##
## The command handler function is not required to take any arguments, but if it does, it should take a single
## `args` argument that is a dictionary of the arguments passed to the command.
var _commandFuncs : Dictionary = {}

#-----------------------------------------------------------------------------------------------------------------------
# Unit Attributes
#-----------------------------------------------------------------------------------------------------------------------

# ## The mesh instance that represents the unit. This is required.
# @export
# var model : RigidBody3D

# Stats

## The maximum health of the unit.
@export_range(0, 1000)
var maxHealth : int = 10

## The current health of the unit.
@export_range(0, 1000)
var health : int = self.maxHealth

## The health at which the unit is considered disabled.
@export_range(0, 1000)
var disabledHealth : int = 0

## The health at which the unit is considered destroyed.
@export_range(0, 1000)
var destroyedHealth : int = 0

#-----------------------------------------------------------------------------------------------------------------------
# Properties
#-----------------------------------------------------------------------------------------------------------------------

var isDisabled : bool:
    get:
        return self.health <= self.disabledHealth and self.health > self.destroyedHealth

var isDestroyed : bool:
    get:
        return self.health <= self.destroyedHealth

#-----------------------------------------------------------------------------------------------------------------------
# Command Handlers
#-----------------------------------------------------------------------------------------------------------------------

func _unknownCommand(command : String, args : Dictionary) -> void:
    prints("Unknown command:", command, "args:", str(args))

#-----------------------------------------------------------------------------------------------------------------------
# Public Functions
#-----------------------------------------------------------------------------------------------------------------------

## Processed a command (from the player) and its arguments. The intent is for this to be how the player controls a
## unit. Each subsclass is expected to register its own command handlers in `_commandFuncs`.
func processCommand(command : String, args : Dictionary = {}) -> void:
    prints("Processing command:", command, "args:", str(args))

    var cmdFunc = self._commandFuncs.get(command)
    if cmdFunc != null:
        cmdFunc.call(args)
    else:
        _unknownCommand(command, args)

#-----------------------------------------------------------------------------------------------------------------------
# Godot Functions
#-----------------------------------------------------------------------------------------------------------------------

func _ready() -> void:
    self.add_to_group("units")

#-----------------------------------------------------------------------------------------------------------------------
