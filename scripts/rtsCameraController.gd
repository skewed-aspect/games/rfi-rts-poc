#-----------------------------------------------------------------------------------------------------------------------
# RTS Style Camera Controller
#-----------------------------------------------------------------------------------------------------------------------

class_name RtsCameraController extends Node3D
## A camera controller for RTS style games.
##
## This camera controller is designed to be used with a Godot 3D scene. It will create two child nodes: an "Elevation"
## node and a "Camera" node. The "Elevation" node will be rotated so that it is facing the ground plane. The "Camera"
## node controls zoom with it's z axis, as well as rotating on it's y axis.
##
## The camera controller supports the following features:
##
## - Movement
## - Rotation
## - Panning
## - Zooming
## - Jumping to a location
## - Locking and unlocking
##
## The camera controller can be locked and unlocked. When the camera controller is locked, it will not respond to
## input events. This is useful for when you want to temporarily disable the camera controller, such as when a UI
## element is being interacted with.
##
## The camera controller can also be jumped to a location. This is useful for when you want to move the camera to a
## specific location, such as when a unit is selected.
##
## The camera controller emits a "cameraMoved" signal when the camera is moved. This is useful for when you want to
## update the position of a UI element that is attached to the camera.

#-----------------------------------------------------------------------------------------------------------------------

var _lastMousePosition : Vector2 = Vector2()
var _isRotating : bool = false
var _isPanning : bool = false
var _isLocked : bool = false
var _zoomDirection : float = 0;
var _tween : Tween

const GROUND_PLANE : Plane = Plane(Vector3.UP, 0)

# Nodes
var _elevationNode : Node3D
var _cameraNode : Camera3D

#-----------------------------------------------------------------------------------------------------------------------
# Signals
#-----------------------------------------------------------------------------------------------------------------------

signal cameraMoved(location : Vector3)

#-----------------------------------------------------------------------------------------------------------------------
# Editor Variables
#-----------------------------------------------------------------------------------------------------------------------

# Movement

@export_range(0, 10000)
var movementSpeed : float = 20

@export_range(0, 500)
var panSpeed = 2

# Rotation

@export_range(0, 90)
var minElevationAngle : int = 10

@export_range(0, 90)
var maxElevationAngle : int = 90

@export_range(0, 100)
var rotationSpeed : float = 0.5

# Zoom

@export_range(0, 10000)
var zoomSpeed : float = 20

@export_range(0, 1000)
var zoomDamping : float = 0.5

@export_range(0, 1000)
var minZoomDistance : int = 10

@export_range(0, 50000)
var maxZoomDistance : int = 500

# Flags

@export
var allowRotation : bool = true

@export
var allowPan : bool = true

@export
var invertYAxis : bool = false

@export
var zoomToCursor : bool = true

#-----------------------------------------------------------------------------------------------------------------------
# Utility Functions
#-----------------------------------------------------------------------------------------------------------------------

func _getMouseDelta() -> Vector2:

    # Get the mouse position
    var currentMousePosition := self.get_viewport().get_mouse_position()

    # Get mouse displacement
    var mouseDisplacement := currentMousePosition - _lastMousePosition

    # Update last mouse position
    _lastMousePosition = currentMousePosition

    return mouseDisplacement

func _getGoundPosition(mousePosition : Vector2) -> Vector3:
    var rayFrom = self._cameraNode.project_ray_origin(mousePosition)
    var rayTo = self._cameraNode.project_ray_normal(mousePosition)

    var newPos = self.GROUND_PLANE.intersects_ray(rayFrom, rayTo)

    if newPos == null:
        return Vector3(INF, INF, INF)
    else:
        return newPos

func _realignCamera(pos : Vector3) -> void:
    var mousePosition = self.get_viewport().get_mouse_position()
    var newPosition = self._getGoundPosition(mousePosition)
    var delta = pos - newPosition

    self._translate(delta)

func _yaw(delta : float, angle : float) -> void:
    self.rotation_degrees.y += angle * delta * rotationSpeed

func _pitch(delta : float, angle : float) -> void:
    var newElevationAngle = self._elevationNode.rotation_degrees.x

    # Support for inverted Y axis
    if invertYAxis:
        newElevationAngle += angle * delta * rotationSpeed
    else:
        newElevationAngle -= angle * delta * rotationSpeed

    # Clamp to the min and max elevation angles
    newElevationAngle = clamp(newElevationAngle, -self.maxElevationAngle, -self.minElevationAngle)

    # Update the elevation angle
    self._elevationNode.rotation_degrees.x = newElevationAngle

func _translate(pos : Vector3) -> void:
    self.position += pos
    self.emit_signal("cameraMoved", position)

func _lock() -> void:
    self._isLocked = true

func _unlock() -> void:
    self._isLocked = false

func _endJump() -> void:
    prints("Jump ended.")
    self._unlock()

#-----------------------------------------------------------------------------------------------------------------------

func _move(delta : float) -> void:
    # Translaton vector
    var velocity := Vector3();

    # Handle input for panning the camera
    if Input.is_action_pressed("cameraForward"):
        velocity -= self.transform.basis.z

    if Input.is_action_pressed("cameraBackward"):
        velocity += self.transform.basis.z

    if Input.is_action_pressed("cameraLeft"):
        velocity -= self.transform.basis.x

    if Input.is_action_pressed("cameraRight"):
        velocity += self.transform.basis.x

    # Calculate translation vector
    velocity = velocity.normalized() * delta * self.movementSpeed

    # Translate the camera
    self._translate(velocity)

func _pan(delta : float) -> void:
    if self.allowPan and self._isPanning:

        # Calculate mouse movement
        var mouseDisplacement := self._getMouseDelta()

        # Calculate the pan velocity
        var velocity = Vector3(mouseDisplacement.x, 0, mouseDisplacement.y) * delta * self.panSpeed

        # Translate the camera
        self._translate(-velocity)

func _rotate(delta : float) -> void:
    if self.allowRotation and self._isRotating:

        # Calculate mouse movement
        var mouseDisplacement := self._getMouseDelta()

        # Calculate yaw and pitch
        self._yaw(delta, mouseDisplacement.x)
        self._pitch(delta, mouseDisplacement.y)

func _zoom(delta : float) -> void:
    # Calculate the new zoom
    var newZoomDistance = clamp(
        self._cameraNode.position.z + (self._zoomDirection * delta * self.zoomSpeed),
        self.minZoomDistance,
        self.maxZoomDistance
    )

    # Save 3D position of the cursor
    var mousePosition = self.get_viewport().get_mouse_position()
    var pointingAt = self._getGoundPosition(mousePosition)

    # Apply the new zoom
    self._cameraNode.position.z = newZoomDistance

    # If zoom to cursor is enabled, move the camera so that the cursor is still pointing at the same 3D position
    if self.zoomToCursor and self._zoomDirection != 0 and pointingAt.is_finite():
        _realignCamera(pointingAt)

    # Dampen the zoom direction
    self._zoomDirection *= self.zoomDamping
    if abs(self._zoomDirection) <= 0.0001:
        self._zoomDirection = 0

func _jumpTo(pos : Vector3, duration : float) -> void:
    if self._tween:
        self._tween.kill()

    self._tween = self.create_tween()
    self._tween.connect("finished", self._endJump)

    # Lock the camera
    self._lock()

    # Tween the camera to the new position
    self._tween.tween_property(self, "position", pos, duration)

#-----------------------------------------------------------------------------------------------------------------------
# Public Functions
#-----------------------------------------------------------------------------------------------------------------------

## Returns the camera node.
func getCamera() -> Camera3D:
    return self._cameraNode

## Retuns the mouse position against the ground plane. If the position does not intersect the ground plane, an infinite
## vector is returned.
func getScreenPositionOnGround(mousePos : Vector2) -> Vector3:
    return self._getGoundPosition(mousePos)

## Returns the elevation node.
func getElevationNode() -> Node3D:
    return self._elevationNode

## Locks the camera, so it will not respond to input events.
func lock() -> void:
    self._lock()

## Unlocks the camera, so it will respond to input events.
func unlock() -> void:
    self._unlock()

## Jumps the camera to the specified position, tweening over the specified duration.
func jumpTo(pos : Vector3, duration : float) -> void:
    self._jumpTo(pos, duration)

#-----------------------------------------------------------------------------------------------------------------------
# Godot Functions
#-----------------------------------------------------------------------------------------------------------------------

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    # Nodes
    self._elevationNode = Node3D.new()
    self._elevationNode.name = "Elevation"
    self._elevationNode.rotation_degrees.x = -45
    self.add_child(self._elevationNode)

    self._cameraNode = Camera3D.new()
    self._cameraNode.name = "Camera"
    self._cameraNode.position.z = 5000
    self._cameraNode.far = 80000
    self._elevationNode.add_child(self._cameraNode)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta : float) -> void:
    if not self._isLocked:
        self._move(delta)
        self._rotate(delta)
        self._zoom(delta)
        self._pan(delta)

# Called when an input event isn't otherwise handled. 'event' is the InputEvent object.
func _unhandled_input(event : InputEvent) -> void:
    if event.is_action_pressed("cameraRotate"):
        self._lastMousePosition = get_viewport().get_mouse_position()
        self._isRotating = true

    if event.is_action_released("cameraRotate"):
        self._isRotating = false

    if event.is_action_pressed("cameraPan"):
        self._lastMousePosition = get_viewport().get_mouse_position()
        self._isPanning = true

    if event.is_action_released("cameraPan"):
        self._isPanning = false

    if event.is_action_pressed("cameraZoomIn"):
        self._zoomDirection = -1

    if event.is_action_pressed("cameraZoomOut"):
        self._zoomDirection = 1

#-----------------------------------------------------------------------------------------------------------------------
