#-----------------------------------------------------------------------------------------------------------------------
# Fleet Mode
#-----------------------------------------------------------------------------------------------------------------------

class_name FleetMode extends Node3D
## The Fleet Mode is the main RTS mode of the game. It handles the camera and the selection of units, as well as level
## selection and loading.

#-----------------------------------------------------------------------------------------------------------------------

const GROUND_PLANE = Plane(Vector3(0, 1, 0), 0)

@onready
var _level : Node3D = $Level

@onready
var _gridMesh : MeshInstance3D = $GridMesh

@onready
var _rtsCamera = RtsCameraController.new()

#-----------------------------------------------------------------------------------------------------------------------
# Signals
#-----------------------------------------------------------------------------------------------------------------------

## Shows the grid in the level
signal gridShow()

## Hides the grid in the level
signal gridHide()

## Jumps the RTS camera to the given location
signal cameraJump(location : Vector3, duration : float)

## Locks the RTS camera to the given location
signal cameraLock()

## Unlocks the RTS camera
signal cameraUnlock()

#-----------------------------------------------------------------------------------------------------------------------
# Event handlers
#-----------------------------------------------------------------------------------------------------------------------

func _onCameraLock() -> void:
    _rtsCamera.lock()

func _onCameraUnlock() -> void:
    _rtsCamera.unlock()

func _onCameraJump(pos : Vector3, duration : float) -> void:
    _rtsCamera.jumpTo(pos, duration)

func _onGridShow() -> void:
    _gridMesh.show()

func _onGridHide() -> void:
    _gridMesh.hide()

#-----------------------------------------------------------------------------------------------------------------------
# Utility Functions
#-----------------------------------------------------------------------------------------------------------------------

func _setupCamera() -> void:
    self._rtsCamera.movementSpeed = 5000
    self._rtsCamera.panSpeed = 500
    self._rtsCamera.rotationSpeed = 10
    self._rtsCamera.zoomSpeed = 5000
    self._rtsCamera.zoomDamping = 0.9
    self._rtsCamera.maxZoomDistance = 10000

    # Add the RTS camera controller to the scene
    self.add_child(_rtsCamera)

#-----------------------------------------------------------------------------------------------------------------------
# Public Methods
#-----------------------------------------------------------------------------------------------------------------------

func loadLevel(level : String) -> void:
    var scene := load(level)

    # Replace the current level node with the new scene
    self._level.replace_by(scene.instantiate())

    # Make out gridmesh match the level size
    self._gridMesh.mesh.size =  Vector2(10000, 10000) # self._level.levelSize # TODO: Figure out how to do this

#-----------------------------------------------------------------------------------------------------------------------
# Godot Methods
#-----------------------------------------------------------------------------------------------------------------------

func _ready() -> void:
    self.add_to_group("modes")

    # Configure the RTS camera
    self._setupCamera()

    # TODO: In the real game, this would happen dynamically. For now, we just hardcode it.
    self.loadLevel("res://levels/asteroid_level.tscn")

    # Events
    self.connect("cameraLock", self._onCameraLock)
    self.connect("cameraUnlock", self._onCameraUnlock)
    self.connect("cameraJump", self._onCameraJump)
    self.connect("gridShow", self._onGridShow)
    self.connect("gridHide", self._onGridHide)

func _unhandled_input(event : InputEvent) -> void:
    if event.is_action_pressed("click"):
        var mousePos = event.position
        var groundPos = self._rtsCamera.getScreenPositionOnGround(mousePos)

        if groundPos != Vector3(INF, INF, INF):
            self.get_tree().call_group("selected", "processCommand", "move", { "pos": groundPos })
#-----------------------------------------------------------------------------------------------------------------------
