#-----------------------------------------------------------------------------------------------------------------------

class_name FreezeButton extends Button

#-----------------------------------------------------------------------------------------------------------------------

var _isFrozen = false

@onready
var fleetMode = self.get_node("/root/FleetMode")

#-----------------------------------------------------------------------------------------------------------------------
# Event Handlers
#-----------------------------------------------------------------------------------------------------------------------

func _onPressed() -> void:
	if self._isFrozen:
		self._isFrozen = false
		fleetMode.emit_signal("cameraUnlock")
		self.text = "Freeze Camera"
	else:
		self._isFrozen = true
		fleetMode.emit_signal("cameraLock")
		self.text = "Unfreeze Camera"

#-----------------------------------------------------------------------------------------------------------------------
# Godot Methods
#-----------------------------------------------------------------------------------------------------------------------

# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect("pressed", self._onPressed)

#-----------------------------------------------------------------------------------------------------------------------
