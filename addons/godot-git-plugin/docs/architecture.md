# Game Architecture

The following is my basic thinking on the game architecture, since Godot (and most other game engines) don't work the same way a more traditional application does. Instead of your code instantiating the engine, the engine instantiates your code. This inversion means that you only have two choices for injecting code for the engine to run:

1) As a script attached to a node in the scene graph
2) As an auto-loaded script at start up

While option #2 is tempting for a lot of things (for example, a top-level game class that controls everything else) it's a little bit magic. Auto-loads are specified in the _project_ which you can find inside the menu of the editor, but isn't obvious just looking at the source code. As such, I would like to use as few (aka 'none') as possible.

So with that out of the way, here's how I'm thinking the structure should go.

## Main Scene

For the purposes of this PoC, the main scene is _just_ the RTS style game. In a full project, I would imagine there would be a `Node` called Game, under which would be a `Node3D` called `FleetMode` and another `Node3D` called `GalaxyMap`. Each of these will be added to a group called `'Modes'`.

Each mode will be configured in it's own way. Since this is just a PoC, we will only have the `FleetMode`. The node structure should look like this:

```
◯ FleetMode
├ ◯ GridMesh
├ ◯ Level
└ ◯ UI
```

The `GridMesh` is just a simple node that contains a PlaneMesh with a grid-drawing shader. This exists so it can be turned on/off (and because doing this in code is actually a bit annoying, so doing it in the editor just worked out.)

The `Level` node is a placeholder `Node3D` that gets replaced with the loaded level. For the PoC we just manually import the asteroids level and call `$Level.replace_by()`. This loads the level into the scene. (Note: Each level will have it's own script to properly set up the scenario.) The level nodes will be added to the group `'Levels'`.

The `UI` node is where the UI will be built. Uncertain if it's better to build it in the editor or via code. Probably the UI should be built in the editor, but the behaviors/etc be backed in code. Will have to work out a good way to do that.

### Levels

Levels will be their own thing, built as they see fit and running code to set themselves up. If there's any need for code on exit, `_exit_tree` can be used.

### Units

Units will be programmatically added to the level (starting units at start up, more via UI calls, game logic, etc). These will be added to a `'Units'` group. Enemies will be added the same way, but to a `'Enemies'` group.

Selected units will be added and removed from a `'Selected'` group, and commands will be sent using `get_tree().call_group("Selected", "command", args)`. (Note: this might need to be turned into a signal, as the docs warn that there might be stuttering since it calls all nodes at once. But that can also be managed by making the command processing asynchronous of some flavor)