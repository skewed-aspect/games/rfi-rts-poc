# RFI RTS PoC

This is a proof of concept for an RTS game in the [RFI Universe](https://rfiuniverse.com). It's an implementation of this:

* https://rfiuniverse.com/en/games/charlemagne/poc

## Getting Started

* Download Godot v4.2
* Check out repo
* Open Godot
* ???
* profit

### Git Plugin and Macs

So, the [godot-git-plugin][] has some issues on mac. Or at least, a one-time-per-checkout issue. Because of security in macOS, you have to 'bless' the binary. Here's the official docs on how to do that:

* [MacOS Workaround](https://github.com/godotengine/godot-git-plugin/issues/58#issuecomment-793451881)

(TL;DR Just right click on `addons/godot-git-plugin/macos/libgit_plugin.macos.editor.universal.dylib` and hit 'Open'. Close the terminal that opens, restart Godot.)

<!-- Links -->

[godot-git-plugin]: https://github.com/godotengine/godot-git-plugin
